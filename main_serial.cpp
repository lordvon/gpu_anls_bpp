#include "debug.h"

#include <cstdio>
#include <iostream>
#include <string>
#include <ctime>

int main(int argc, char *argv[])
{
	srand(10);
	if(argc < 4)
	{
		std::cout << "Input arguments:\n\t1: Filename for A matrix (as in A ~= WH)\n\t2: New desired dimension\n\t3: Max NMF iterations\n\t4: Max NNLS iterations\n\t5 (optional): delimiter (space is default)\n";
	}
	else
	{
		std::string filename = argv[1];
		int newDimension = atoi(argv[2]);
		int max_iter_nmf = atoi(argv[3]);
		int max_iter_nnls = atoi(argv[4]);
		char delimiter = (argc > 5) ? *argv[5] : ' ';

		DenseMatrix* A = readMatrix(filename,delimiter);
		A->copyColumnToRow();
		printf("Sparsity of A: %f\n",sparsity(A));

		DenseMatrix W = DenseMatrix(A->rows,newDimension);
		DenseMatrix H = DenseMatrix(newDimension,A->cols);
		NMF_Input input = NMF_Input(&W,&H,A,max_iter_nmf,max_iter_nnls);

		std::cout << "Starting NMF computation." << std::endl;
		std::clock_t start = std::clock();
		double duration;
		// nmf_cpu(input);
		nmf_cpu_profile(input);
		duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
		std::cout << "NMF computation complete. Time: " << duration << " s." << std::endl;

		W.copyColumnToRow();
		dtype AF = FrobeniusNorm(A);
		dtype WH_AF = Fnorm(W,H,*A);
		printf("Objective value: %f\n",WH_AF/AF);
		if(A) delete A;
	}
	return 0;
}