To compile the serial version, simply compile the main_serial.cpp file (or use ./compile_serial).

To compile the parallel version, simply use 'make'. Be sure that the cuda path and architecture is properly set in Makefile.

To see usage options explained in detail, simply run './nmf_serial' or './nmf_parallel' (whichever you compiled).

Usage:
./nmf_parallel data/arcene_test.data 50 10 50
./nmf_serial data/arcene_test.data 50 10 50